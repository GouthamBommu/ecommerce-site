import React from "react";

import Product from "./Product";

import "./Home.css";

function Home() {
  return (
    <div className="home">
      <img
        className="home__image"
        src="https://images-eu.ssl-images-amazon.com/images/G/02/digital/video/merch2016/Hero/Covid19/Generic/GWBleedingHero_ENG_COVIDUPDATE__XSite_1500x600_PV_en-GB._CB428684220_.jpg"
        alt=""
      />
      <div className="home__row">
        <Product
          id={1}
          title={"Concept of physics: Basics of physics by HC verma"}
          image={
            "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1352225642l/16133355.jpg"
          }
          price={50}
          rating={5}
        />
        <Product
          id={2}
          title={"Fossil hybrid watch"}
          image={
            "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSHgCj5Z-dTyUoAHO3Zw2uwrcywrFuxp-GpcWAqH6jpUcI_S0V0IQz07rA7bns5gQeMpkNigYyT&usqp=CAc"
          }
          price={150}
          rating={3}
        />
      </div>
      <div className="home__row">
        <Product
          id={3}
          title={"Surface laptop 3 - 13.5, Black"}
          image={
            "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRM-zt9zcm6igbt4wNfELon1OnJXS6W7oxKgi-u28cRJGLKT6I5iuJe74evPJehSnCj73fEoA&usqp=CAc"
          }
          price={1349}
          rating={4}
        />
      </div>
    </div>
  );
}

export default Home;
