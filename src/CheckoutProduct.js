import React from "react";
import { useStateValue } from "./StateProvider";

import "./CheckoutProduct.css";

function CheckoutProduct({ id, title, image, price, rating }) {
  const [state, dispatch] = useStateValue();
  const removeItemFromBasket = () => {
    dispatch({
      type: "REMOVE_FROM_BASKET",
      item: {
        id: id,
        title: title,
        image: image,
        price: price,
        rating: rating,
      },
    });
  };
  return (
    <div className="checkoutProduct">
      <img src={image} alt="" />
      <div className="checkoutProduct__info">
        <p className="checkoutProduct__title">{title}</p>
        <p className="checkoutProduct__price">
          <small>$</small>
          <strong>{price}</strong>
        </p>
        <div className="checkoutProduct__rating">
          {Array(rating)
            .fill()
            .map((_) => (
              <p>&#9733;</p>
            ))}
        </div>
        <button onClick={removeItemFromBasket}>Remove from basket</button>
      </div>
    </div>
  );
}

export default CheckoutProduct;
