export const initialState = {
  basket: [],
  user: null,
};

function reducer(state, action) {
  switch (action.type) {
    case "SET_USER":
      // login
      return { ...state, user: action.user };
    case "ADD_TO_BASKET":
      // logic for adding item to basket
      return { ...state, basket: [...state.basket, action.item] };
    case "REMOVE_FROM_BASKET":
      // logic to remove item from basket
      let newBasket = [...state.basket];

      let itemIndex = state.basket.findIndex(
        (item) => (item.id = action.item.id)
      );

      if (itemIndex >= 0) {
        newBasket.splice(itemIndex, 1);
      } else {
        console.warn(`Can't remove product (id: ${action.item.id})`);
      }

      return {
        ...state,
        basket: [...newBasket],
      };
    default:
      return state;
  }
}

export default reducer;
