import firebase from "firebase";

const firbaseApp = firebase.initializeApp({
  apiKey: "AIzaSyCQepBVELUF_mtCXqMc_izBxdRAS3tcosI",
  authDomain: "ecommerce-site-86bc6.firebaseapp.com",
  databaseURL: "https://ecommerce-site-86bc6.firebaseio.com",
  projectId: "ecommerce-site-86bc6",
  storageBucket: "ecommerce-site-86bc6.appspot.com",
  messagingSenderId: "968027782227",
  appId: "1:968027782227:web:0efcb940e7381695865aa6",
});

const db = firbaseApp.firestore();
const auth = firbaseApp.auth();

export { db, auth };
